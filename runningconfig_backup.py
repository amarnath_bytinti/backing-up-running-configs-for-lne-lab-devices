from napalm import get_network_driver
import csv
import os
from time import strftime
import time
from threading import Thread
import sys
from netmiko import ConnectHandler

#this is the function used for all OS types except for 'nxos'
def getrunconf(input):
	#fetching the proper network driver
	driver = get_network_driver(str(input['OS']))
	try:
		device = driver(str(input['SSHIP']), str(input['username']),str(input['pass']))  # connecting to device via ssh with details provided by user
		device.open()  # opening the device
		print("Connection to device " + str(input['Devicename']) + " is successful")
	except:
		# printing below statement if in case we are unable to connect to the device using ssh
		print("Connection to device " + str(input['Devicename']) + " failed")
		quit()
	try:
		config = device.get_config()
		#selecting 'running' config
		conf = config['running']
		#since running-configurations on our devices are huge, we need to wait for sometime to get it, here it is 5 seconds
		time.sleep(10)		#sleeping for 10 sec, just to make sure that we grab whole running-config
	except:
		print("Error while grabbing running config from device "+str(input['Devicename']))
		device.close()
		quit()
	#using 'strftime' from 'time' module to get the current timestamp
	timestamp = strftime("%Y-%m-%d_%H_%M_%S")
	#naming the file where run-conf will be stored as 'Devicename_timestamp.txt'
	conffile = str(input['Devicename'])+"_"+str(timestamp)+".txt"
	#opening the txt file to write the run-conf into it
	runconf = open(conffile,"wb")
	device.close()
	try:
		#iterating through the run-conf obtained and writing them onto txt file just created
		for line in conf:
			runconf.write(line.encode('utf-8'))			#since we are getting error in decoding the running-config for Junos, I kept utf-8 for every OS, just to maintain uniformity
		print("Running config file for "+str(input['Devicename'])+" saved successfully")
	except:
		#in case of any errors while writing to txt file
		print("Running config file for "+str(input['Devicename'])+" not saved, some issue is there")
		runconf.close()
		quit()
	#closing the txt file
	runconf.close()

#this is the function used for 'nxos' devices, we use netmiko here
def nxrunconf(input):
	#since netwmiko requires the user inputs in dictionary format, converting the user given input in dictionaries
	creds = {}
	creds[str(input['Devicename'])] = {}
	creds[str(input['Devicename'])]['device_type'] = 'cisco_nxos'
	creds[str(input['Devicename'])]['ip'] = str(input['SSHIP'])
	creds[str(input['Devicename'])]['username'] = str(input['username'])
	creds[str(input['Devicename'])]['password'] = str(input['pass'])
	try:
		#selecting the proper dictionary for this thread, based on device name
		device = creds[str(input['Devicename'])]
		#connecting to the device via SSH using netmiko's ConnectHandler
		net_connect = ConnectHandler(**device)
		print("Connection to device " + str(input['Devicename']) + " is successful")
	except:
		#in case some error occurs while connecting
		print("Connection to device " + str(input['Devicename']) + " failed")
		quit()
	try:
		#sending 'show run' command to get the running-configuration
		conf = net_connect.send_command("show run")
		time.sleep(10)			#sleeping for 10 sec, just to make sure that we grab whole running-config
	except:
		print("Error while grabbing running config from device " + str(input['Devicename']))
		net_connect.disconnect()			#disconnecting from device
		quit()
	timestamp = strftime("%Y-%m-%d_%H_%M_%S")
	# naming the file where run-conf will be stored as 'Devicename_timestamp.txt'
	conffile = str(input['Devicename']) + "_" + str(timestamp) + ".txt"
	# opening the txt file to write the run-conf into it
	runconf = open(conffile, "wb")
	net_connect.disconnect()			#disconnecting from device, net_connect here
	time.sleep(1)
	try:
		#saving the running-config into txt file
		for line in conf:
			runconf.write(line.encode('utf-8'))				#since we are getting error in decoding the running-config for Junos, I kept utf-8 for every OS, just to maintain uniformity
		print("Running config file for "+str(input['Devicename'])+" saved successfully")
		runconf.close()			#closing the txt file
	except:
		print("Running config file for " + str(input['Devicename']) + " not saved, some issue is there")
		runconf.close()
		quit()

print("**WARNING!!** Make sure that you have entered the filename in arguments!**")
#to check whether user has given the input filename in command line
if len(sys.argv) != 2:
	print("Filename missing, please enter the filename in arguments")
	sys.exit()			#exiting the code since the input filename is missing
else:
	filename = sys.argv[1]
#checking the presence of input file given by user in CWD
if os.path.isfile(filename) == 0:
	print("File is not there in specified path, please check!!")
	exit()
with open(filename,'r') as file:		#opening file with read permissions
	csvread = csv.DictReader(file)		#reading csv file in dictionary format
	i = 0			#initializing a counter which will be incremented in below 'for' loop
	final = []		#initializing a list by name 'final', which contains multiple dictionaries
	#iterating through the CSV file and making dictionaries for every device with all its login details
	for ele in csvread:
		dict = 'dict'+str(i)
		dict = {}				#initializing one dictionary per device in input file
		#assigning the inputs/login details to keys for each dictionary, like IP,username,password etc.
		dict['Devicename']=ele['Device']
		dict['SSHIP'] = ele['SSH IP']
		if ele['OS'] == 'ios' or ele['OS'] == 'iosxr' or ele['OS'] == 'junos' or ele['OS'] == 'nxos':
			dict['OS'] = ele['OS']
		else:
			print("Wrong OS type entry in input file for device "+ str(ele['Device']))
		dict['username'] = ele['Username']
		dict['pass'] = ele['Password']
		final.append(dict)		#appending each and every dictionary with login details, to the list 'final'(list of dictionaries)
		i = i+1

threads = []
#iterating through the devices as per user input
for input in final:
	#checking whether OS type is 'nxos'
	if str(input['OS']) == 'nxos':
		#if OS type is 'nxos', we are calling 'nxrunconf' function, which uses netmiko, not napalm
		inputrunconf = Thread(target=nxrunconf, args=[input,])
		#starting the thread
		inputrunconf.start()
	else:
		#initializing the thread for other OS types, here function we are calling is 'getrunconf', it uses napalm
		inputrunconf = Thread(target=getrunconf, args=[input,])
		inputrunconf.start()
	#appending all single threads into a list by name 'threads'
	threads.append(inputrunconf)

#joining the threads
for thread in threads:
	thread.join()